import createHttpError from 'http-errors';
import { create, read, update, deleteitem } from '../models/food.model.js';
import { codes } from '../config.js';
import { handleAnswer } from '../middleware/answerHandler.js';

function createData(req, res, next) {
	const data = req.body;
	if (JSON.stringify(data) === '{}') {
		return next(createHttpError(codes['BAD REQUEST'], 'Data wasnt provided'));
	}

	create(
		results => handleAnswer(res, codes['OK'], results),
		err => next(createHttpError(codes['Internal Server Error'], err)),
		data
	);
}

function readData(req, res, next) {
	read(
		results => handleAnswer(res, codes['OK'], results),
		err => next(createHttpError(codes['Internal Server Error'], err))
	);
}

function readItem(req, res, next) {
	read(
		results => handleAnswer(res, codes['OK'], results),
		err => next(createHttpError(codes['Internal Server Error'], err)),
		req.params.id
	);
}

function updateData(req, res, next) {
	const data = req.body;

	if (JSON.stringify(data) === '{}') {
		return next(createHttpError(codes['BAD REQUEST'], 'Data wasnt provided'));
	}

	update(
		results => handleAnswer(res, codes['OK'], results),
		err => next(createHttpError(codes['Internal Server Error'], err)),
		data,
		req.params.id
	);
}

function deleteData(req, res, next) {
	deleteitem(
		results => handleAnswer(res, codes['OK'], results),
		err => next(createHttpError(codes['Internal Server Error'], err)),
		req.params.id
	);
}

export { createData, updateData, readData, readItem, deleteData };
