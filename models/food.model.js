import { connection } from '../db/connect.js';
import {
	createsql,
	updateSql,
	readsql,
	readsqlitem,
	deletesql,
} from '../db/helpers/foodsql.js';

function create(sendOk, sendBad, values) {
	connection.query(createsql(), [values], (err, results) => {
		if (err) sendBad(err);
		else sendOk(values);
	});
}

function update(sendOk, sendBad, values, id) {
	connection.query(updateSql(), [values, id], (err, results) => {
		if (err) sendBad(err);
		else sendOk(values);
	});
}

function read(sendOk, sendBad, id) {
	if (!id) {
		connection.query(readsql(), (err, results) => {
			if (err) sendBad(err);
			else sendOk(results);
		});
	} else {
		connection.query(readsqlitem(), [id], (err, results) => {
			if (err) sendBad(err);
			else sendOk(results);
		});
	}
}

function deleteitem(sendOk, sendBad, id) {
	connection.query(deletesql(), [id], (err, results) => {
		if (err) sendBad(err);
		else sendOk(results.affectedRows + ' rows was deleted');
	});
}

export { create, update, read, deleteitem };
