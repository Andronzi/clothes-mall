import { tables } from '../../config.js';
import { connection } from '../connect.js';

function dropTable() {
	const sql = `DROP TABLE IF EXISTS ${tables.food}`;
	connection.query(sql, err => {
		if (err) throw err;
		console.log('table dropped');
	});
}

function createTable() {
	const sql = `CREATE TABLE IF NOT EXISTS ${tables.food}
    (id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT, name VARCHAR(255) NOT NULL, 
	note TEXT(2000) NOT NULL, image TEXT(1000), category VARCHAR(255) NOT NULL)`;
	connection.query(sql, err => {
		if (err) throw err;
		console.log('table created');
	});
}

function createTables() {
	createTable();
}

export default createTables;
