import { tables } from '../../config.js';

function createsql() {
	return `INSERT INTO ${tables.food} SET ?`;
}

function updateSql() {
	return `UPDATE ${tables.food} SET ? WHERE id = ?`;
}

function readsql() {
	return `SELECT * FROM ${tables.food}`;
}

function readsqlitem() {
	return `SELECT * FROM ${tables.food} WHERE id = ?`;
}

function deletesql() {
	return `DELETE FROM ${tables.food} WHERE id = ?`;
}

export { createsql, updateSql, readsql, readsqlitem, deletesql };
