import express from 'express';
import {
	createData,
	updateData,
	readData,
	readItem,
	deleteData,
} from '../controllers/food.js';

const foodRouter = express.Router();
foodRouter.post('/create', createData);
foodRouter.get('/', readData);
foodRouter.get('/:id', readItem);
foodRouter.put('/edit/:id', updateData);
foodRouter.get('/delete/:id', deleteData);

export default foodRouter;
