import express from 'express';
import createHttpError from 'http-errors';
import dotenv from 'dotenv';
import foodRouter from './routes/foodRouter.js';
import { codes } from './config.js';
import { connect } from './db/connect.js';
import { handleError } from './middleware/answerHandler.js';

const app = express();
dotenv.config();
app.use(express.json());
app.use(
	express.urlencoded({
		extended: true,
	})
);

app.use('/api/food', foodRouter);
app.use((req, res, next) => {
	next(createHttpError(codes['Not Found'], 'Страница не найдена'));
});

app.use((error, req, res, next) => {
	return handleError(res, error.status || 500, error);
});

app.listen(process.env.PORT, async () => {
	await connect();
});

// normal request
// http://localhost:3000/food/create?fields=name, price&values='Adidasik'&values=1243
